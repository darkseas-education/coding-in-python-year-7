# Coding in Python Year 7

JupyterLite deployed as a static site to GitLab Pages, for demo purposes.

## ✨ Try it in your browser ✨

➡️ **https://benabel.gitlab.io/jupyterlite-template**

**https://darkseas-education.gitlab.io/coding-in-python-year-7/**
